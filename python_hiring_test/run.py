"""Main script for generating output.csv."""

import pandas as pd


def main():
    # add basic program logic here
    """
    df1 = AVG, HitterID, vs RHP
    df6 = AVG, HitterID, vs LHP
    
    
    #AVG = H/AB
    #OBP = (H + BB+ HBP)/(AB + BB + HBP + SF)
    #SLG = H + 2B + (2 x 3B) + (3 x HR) / AB    
    #OPS = OBP + SLG
    
    
    """
    ################ Hitter stats #######################
    
    df = pd.read_csv('./data/raw/pitchdata.csv')
    #finding the AVG df1 using AVG = H/AB
    df_AVG = df.groupby(['HitterId','PitcherSide'], as_index=False)['H','AB','PitcherSide', 'PA'].sum()
    df_AVG = df_AVG[df_AVG['PA']>24]
    df_AVG_RHP = df_AVG[df_AVG['PitcherSide']=='R']
    df_AVG_LHP = df_AVG[df_AVG['PitcherSide']=='L']
       
    df_OBP = df.groupby(['HitterId','PitcherSide'], as_index=False)['H','AB','PitcherSide', 'BB', 'HBP', 'SF', 'PA'].sum()
    df_OBP = df_OBP[df_OBP['PA']>24]
    df_OBP_RHP = df_OBP[df_OBP['PitcherSide']=='R']
    df_OBP_LHP = df_OBP[df_OBP['PitcherSide']=='L']
      
    df_SLG = df.groupby(['HitterId','PitcherSide'], as_index=False)['H','AB','2B','3B','HR','PitcherSide', 'PA'].sum()
    df_SLG = df_SLG[df_SLG['PA']>24]
    df_SLG_RHP = df_SLG[df_SLG['PitcherSide']=='R']
    df_SLG_LHP = df_SLG[df_SLG['PitcherSide']=='L']
    
    df_AVG_LHP['AVG'] = df_AVG_LHP['H']/df_AVG_LHP['AB']
    df_AVG_RHP['AVG'] = df_AVG_RHP['H']/df_AVG_RHP['AB']
    
    df_OBP_RHP['OBP'] = (df_OBP_RHP['H'] + df_OBP_RHP['BB'] + 
                         df_OBP_RHP['HBP'])/(df_OBP_RHP['AB'] + df_OBP_RHP['BB']
                                            + df_OBP_RHP['HBP'] + df_OBP_RHP['SF'])
    df_OBP_LHP['OBP'] = (df_OBP_LHP['H'] + df_OBP_LHP['BB'] + 
                         df_OBP_LHP['HBP'])/(df_OBP_LHP['AB'] + df_OBP_LHP['BB']
                                            + df_OBP_LHP['HBP'] + df_OBP_LHP['SF'])
    
    df_SLG_RHP['SLG'] = (df_SLG_RHP['H'] + df_SLG_RHP['2B'] + 
                         2*df_SLG_RHP['3B'] + 3*df_SLG_RHP['HR'])/df_SLG_RHP['AB'] 
    df_SLG_LHP['SLG'] = (df_SLG_LHP['H'] + df_SLG_LHP['2B'] + 
                         2*df_SLG_LHP['3B'] + 3*df_SLG_LHP['HR'])/df_SLG_LHP['AB'] 
    
    df_OPS_LHP = df_OBP_LHP
    df_OPS_RHP = df_OBP_RHP
    
    df_OPS_LHP['OPS'] = df_OBP_LHP['OBP'] + df_SLG_LHP['SLG']
    df_OPS_RHP['OPS'] = df_OBP_RHP['OBP'] + df_SLG_RHP['SLG']

    columnsList_AVG_RHP = ['AVG','HitterId','PitcherSide']
    columnsList_AVG_LHP = ['AVG','HitterId','PitcherSide']
    
    columnsList_OBP_RHP = ['OBP','HitterId','PitcherSide']
    columnsList_OBP_LHP = ['OBP','HitterId','PitcherSide']
    
    columnsList_SLG_RHP = ['SLG','HitterId','PitcherSide']
    columnsList_SLG_LHP = ['SLG','HitterId','PitcherSide']
    
    columnsList_OPS_LHP = ['OPS','HitterId','PitcherSide']
    columnsList_OPS_RHP = ['OPS','HitterId','PitcherSide']
    
    
    df_AVG_LHP = df_AVG_LHP.reindex(columns=columnsList_AVG_LHP).round(3)
    df_AVG_RHP = df_AVG_RHP.reindex(columns=columnsList_AVG_RHP).round(3)
    
    df_OBP_LHP = df_OBP_LHP.reindex(columns=columnsList_OBP_LHP).round(3)
    df_OBP_RHP = df_OBP_RHP.reindex(columns=columnsList_OBP_RHP).round(3)
    
    df_SLG_LHP = df_SLG_LHP.reindex(columns=columnsList_SLG_LHP).round(3) 
    df_SLG_RHP = df_SLG_RHP.reindex(columns=columnsList_SLG_RHP).round(3)
    
    
    df_OPS_LHP = df_OPS_LHP.reindex(columns=columnsList_OPS_LHP).round(3)
    df_OPS_RHP = df_OPS_RHP.reindex(columns=columnsList_OPS_RHP).round(3)

    
    ################### Hitter team stats ####################
    
    
    HT_AVG = df.groupby(['HitterTeamId','PitcherSide'], as_index=False)['H','AB','PitcherSide', 'PA'].sum()
    HT_AVG = HT_AVG[HT_AVG['PA']>24]
    HT_AVG_RHP = HT_AVG[HT_AVG['PitcherSide']=='R']
    HT_AVG_LHP = HT_AVG[HT_AVG['PitcherSide']=='L']
    
    HT_OBP = df.groupby(['HitterTeamId','PitcherSide'], as_index=False)['H','AB','PitcherSide', 'BB', 'HBP', 'SF', 'PA'].sum()
    HT_OBP = HT_OBP[HT_OBP['PA']>24]
    HT_OBP_RHP = HT_OBP[HT_OBP['PitcherSide']=='R']
    HT_OBP_LHP = HT_OBP[HT_OBP['PitcherSide']=='L']
    
    HT_SLG = df.groupby(['HitterTeamId','PitcherSide'], as_index=False)['H','AB','2B','3B','HR','PitcherSide', 'PA'].sum()
    HT_SLG = HT_SLG[HT_SLG['PA']>24]
    HT_SLG_RHP = HT_SLG[HT_SLG['PitcherSide']=='R']
    HT_SLG_LHP = HT_SLG[HT_SLG['PitcherSide']=='L']
    
    HT_AVG_LHP['AVG'] = HT_AVG_LHP['H']/HT_AVG_LHP['AB']
    HT_AVG_RHP['AVG'] = HT_AVG_RHP['H']/HT_AVG_RHP['AB']
    	
    HT_OBP_RHP['OBP'] = (HT_OBP_RHP['H'] + HT_OBP_RHP['BB'] + 
                         HT_OBP_RHP['HBP'])/(HT_OBP_RHP['AB'] + HT_OBP_RHP['BB']
                                            + HT_OBP_RHP['HBP'] + HT_OBP_RHP['SF'])
    HT_OBP_LHP['OBP'] = (HT_OBP_LHP['H'] + HT_OBP_LHP['BB'] + 
                         HT_OBP_LHP['HBP'])/(HT_OBP_LHP['AB'] + HT_OBP_LHP['BB']
                                            + HT_OBP_LHP['HBP'] + HT_OBP_LHP['SF'])
    
    HT_SLG_RHP['SLG'] = (HT_SLG_RHP['H'] + HT_SLG_RHP['2B'] + 
                         2*HT_SLG_RHP['3B'] + 3*HT_SLG_RHP['HR'])/HT_SLG_RHP['AB'] 
    HT_SLG_LHP['SLG'] = (HT_SLG_LHP['H'] + HT_SLG_LHP['2B'] + 
                         2*HT_SLG_LHP['3B'] + 3*HT_SLG_LHP['HR'])/HT_SLG_LHP['AB'] 
    
    HT_OPS_LHP = HT_OBP_LHP
    HT_OPS_RHP = HT_OBP_RHP
    
    HT_OPS_LHP['OPS'] = HT_OBP_LHP['OBP'] + HT_SLG_LHP['SLG']
    HT_OPS_RHP['OPS'] = HT_OBP_RHP['OBP'] + HT_SLG_RHP['SLG']
    
    columnsList_AVG_RHP = ['AVG','HitterTeamId','PitcherSide']
    columnsList_AVG_LHP = ['AVG','HitterTeamId','PitcherSide']
    
    columnsList_OBP_RHP = ['OBP','HitterTeamId','PitcherSide']
    columnsList_OBP_LHP = ['OBP','HitterTeamId','PitcherSide']
    
    columnsList_SLG_RHP = ['SLG','HitterTeamId','PitcherSide']
    columnsList_SLG_LHP = ['SLG','HitterTeamId','PitcherSide']
    
    columnsList_OPS_LHP = ['OPS','HitterTeamId','PitcherSide']
    columnsList_OPS_RHP = ['OPS','HitterTeamId','PitcherSide']
    
    HT_AVG_LHP = HT_AVG_LHP.reindex(columns=columnsList_AVG_LHP).round(3)
    HT_AVG_RHP = HT_AVG_RHP.reindex(columns=columnsList_AVG_RHP).round(3)
    
    HT_OBP_LHP = HT_OBP_LHP.reindex(columns=columnsList_OBP_LHP).round(3)
    HT_OBP_RHP = HT_OBP_RHP.reindex(columns=columnsList_OBP_RHP).round(3)
    
    
    HT_SLG_LHP = HT_SLG_LHP.reindex(columns=columnsList_SLG_LHP).round(3) 
    HT_SLG_RHP = HT_SLG_RHP.reindex(columns=columnsList_SLG_RHP).round(3)
    
    
    HT_OPS_LHP = HT_OPS_LHP.reindex(columns=columnsList_OPS_LHP).round(3)
    HT_OPS_RHP = HT_OPS_RHP.reindex(columns=columnsList_OPS_RHP).round(3)

      
    ################### Pitcher stats ####################
    
    PI_AVG = df.groupby(['PitcherId','HitterSide'], as_index=False)['H','AB','HitterSide', 'PA'].sum()
    PI_AVG = PI_AVG[PI_AVG['PA']>24]
    PI_AVG_RHH = PI_AVG[PI_AVG['HitterSide']=='R']
    PI_AVG_LHH = PI_AVG[PI_AVG['HitterSide']=='L']
       
    PI_OBP = df.groupby(['PitcherId','HitterSide'], as_index=False)['H','AB','HitterSide', 'BB', 'HBP', 'SF', 'PA'].sum()
    PI_OBP = PI_OBP[PI_OBP['PA']>24]
    PI_OBP_RHH = PI_OBP[PI_OBP['HitterSide']=='R']
    PI_OBP_LHH = PI_OBP[PI_OBP['HitterSide']=='L']
      
    PI_SLG = df.groupby(['PitcherId','HitterSide'], as_index=False)['H','AB','2B','3B','HR','HitterSide', 'PA'].sum()
    PI_SLG = PI_SLG[PI_SLG['PA']>24]
    PI_SLG_RHH = PI_SLG[PI_SLG['HitterSide']=='R']
    PI_SLG_LHH = PI_SLG[PI_SLG['HitterSide']=='L']
    
    PI_AVG_LHH['AVG'] = PI_AVG_LHH['H']/PI_AVG_LHH['AB']
    PI_AVG_RHH['AVG'] = PI_AVG_RHH['H']/PI_AVG_RHH['AB']
    
    PI_OBP_RHH['OBP'] = (PI_OBP_RHH['H'] + PI_OBP_RHH['BB'] + 
                         PI_OBP_RHH['HBP'])/(PI_OBP_RHH['AB'] + PI_OBP_RHH['BB']
                                            + PI_OBP_RHH['HBP'] + PI_OBP_RHH['SF'])
    PI_OBP_LHH['OBP'] = (PI_OBP_LHH['H'] + PI_OBP_LHH['BB'] + 
                         PI_OBP_LHH['HBP'])/(PI_OBP_LHH['AB'] + PI_OBP_LHH['BB']
                                            + PI_OBP_LHH['HBP'] + PI_OBP_LHH['SF'])
    
    PI_SLG_RHH['SLG'] = (PI_SLG_RHH['H'] + PI_SLG_RHH['2B'] + 
                         2*PI_SLG_RHH['3B'] + 3*PI_SLG_RHH['HR'])/PI_SLG_RHH['AB'] 
    PI_SLG_LHH['SLG'] = (PI_SLG_LHH['H'] + PI_SLG_LHH['2B'] + 
                         2*PI_SLG_LHH['3B'] + 3*PI_SLG_LHH['HR'])/PI_SLG_LHH['AB'] 
    
    PI_OPS_LHH = PI_OBP_LHH
    PI_OPS_RHH = PI_OBP_RHH
    
    PI_OPS_LHH['OPS'] = PI_OBP_LHH['OBP'] + PI_SLG_LHH['SLG']
    PI_OPS_RHH['OPS'] = PI_OBP_RHH['OBP'] + PI_SLG_RHH['SLG']

    columnsList_AVG_RHH = ['AVG','PitcherId','HitterSide']
    columnsList_AVG_LHH = ['AVG','PitcherId','HitterSide']
    
    columnsList_OBP_RHH = ['OBP','PitcherId','HitterSide']
    columnsList_OBP_LHH = ['OBP','PitcherId','HitterSide']
    
    columnsList_SLG_RHH = ['SLG','PitcherId','HitterSide']
    columnsList_SLG_LHH = ['SLG','PitcherId','HitterSide']
    
    columnsList_OPS_LHH = ['OPS','PitcherId','HitterSide']
    columnsList_OPS_RHH = ['OPS','PitcherId','HitterSide']
    
    PI_AVG_LHH = PI_AVG_LHH.reindex(columns=columnsList_AVG_LHH).round(3)
    PI_AVG_RHH = PI_AVG_RHH.reindex(columns=columnsList_AVG_RHH).round(3)
    
    PI_OBP_LHH = PI_OBP_LHH.reindex(columns=columnsList_OBP_LHH).round(3)
    PI_OBP_RHH = PI_OBP_RHH.reindex(columns=columnsList_OBP_RHH).round(3)
    
    PI_SLG_LHH = PI_SLG_LHH.reindex(columns=columnsList_SLG_LHH).round(3) 
    PI_SLG_RHH = PI_SLG_RHH.reindex(columns=columnsList_SLG_RHH).round(3)
    
    
    PI_OPS_LHH = PI_OPS_LHH.reindex(columns=columnsList_OPS_LHH).round(3)
    PI_OPS_RHH = PI_OPS_RHH.reindex(columns=columnsList_OPS_RHH).round(3)


    ################### Pitcher team stats ####################
    
    PT_AVG = df.groupby(['PitcherTeamId','HitterSide'], as_index=False)['H','AB','HitterSide', 'PA'].sum()
    PT_AVG = PT_AVG[PT_AVG['PA']>24]
    PT_AVG_RHH = PT_AVG[PT_AVG['HitterSide']=='R']
    PT_AVG_LHH = PT_AVG[PT_AVG['HitterSide']=='L']
       
    PT_OBP = df.groupby(['PitcherTeamId','HitterSide'], as_index=False)['H','AB','HitterSide', 'BB', 'HBP', 'SF', 'PA'].sum()
    PT_OBP = PT_OBP[PT_OBP['PA']>24]
    PT_OBP_RHH = PT_OBP[PT_OBP['HitterSide']=='R']
    PT_OBP_LHH = PT_OBP[PT_OBP['HitterSide']=='L']
      
    PT_SLG = df.groupby(['PitcherTeamId','HitterSide'], as_index=False)['H','AB','2B','3B','HR','HitterSide', 'PA'].sum()
    PT_SLG = PT_SLG[PT_SLG['PA']>24]
    PT_SLG_RHH = PT_SLG[PT_SLG['HitterSide']=='R']
    PT_SLG_LHH = PT_SLG[PT_SLG['HitterSide']=='L']
    
    PT_AVG_LHH['AVG'] = PT_AVG_LHH['H']/PT_AVG_LHH['AB']
    PT_AVG_RHH['AVG'] = PT_AVG_RHH['H']/PT_AVG_RHH['AB']
    
    PT_OBP_RHH['OBP'] = (PT_OBP_RHH['H'] + PT_OBP_RHH['BB'] + 
                         PT_OBP_RHH['HBP'])/(PT_OBP_RHH['AB'] + PT_OBP_RHH['BB']
                                            + PT_OBP_RHH['HBP'] + PT_OBP_RHH['SF'])
    PT_OBP_LHH['OBP'] = (PT_OBP_LHH['H'] + PT_OBP_LHH['BB'] + 
                         PT_OBP_LHH['HBP'])/(PT_OBP_LHH['AB'] + PT_OBP_LHH['BB']
                                            + PT_OBP_LHH['HBP'] + PT_OBP_LHH['SF'])
    
    PT_SLG_RHH['SLG'] = (PT_SLG_RHH['H'] + PT_SLG_RHH['2B'] + 
                         2*PT_SLG_RHH['3B'] + 3*PT_SLG_RHH['HR'])/PT_SLG_RHH['AB'] 
    PT_SLG_LHH['SLG'] = (PT_SLG_LHH['H'] + PT_SLG_LHH['2B'] + 
                         2*PT_SLG_LHH['3B'] + 3*PT_SLG_LHH['HR'])/PT_SLG_LHH['AB'] 
    
    PT_OPS_LHH = PT_OBP_LHH
    PT_OPS_RHH = PT_OBP_RHH
    
    PT_OPS_LHH['OPS'] = PT_OBP_LHH['OBP'] + PT_SLG_LHH['SLG']
    PT_OPS_RHH['OPS'] = PT_OBP_RHH['OBP'] + PT_SLG_RHH['SLG']

    
    columnsList_AVG_RHH = ['AVG','PitcherTeamId','HitterSide']
    columnsList_AVG_LHH = ['AVG','PitcherTeamId','HitterSide']
    
    columnsList_OBP_RHH = ['OBP','PitcherTeamId','HitterSide']
    columnsList_OBP_LHH = ['OBP','PitcherTeamId','HitterSide']
    
    columnsList_SLG_RHH = ['SLG','PitcherTeamId','HitterSide']
    columnsList_SLG_LHH = ['SLG','PitcherTeamId','HitterSide']
    
    columnsList_OPS_LHH = ['OPS','PitcherTeamId','HitterSide']
    columnsList_OPS_RHH = ['OPS','PitcherTeamId','HitterSide']
    
    PT_AVG_LHH = PT_AVG_LHH.reindex(columns=columnsList_AVG_LHH).round(3)
    PT_AVG_RHH = PT_AVG_RHH.reindex(columns=columnsList_AVG_RHH).round(3)
    
    PT_OBP_LHH = PT_OBP_LHH.reindex(columns=columnsList_OBP_LHH).round(3)
    PT_OBP_RHH = PT_OBP_RHH.reindex(columns=columnsList_OBP_RHH).round(3)
    
    PT_SLG_LHH = PT_SLG_LHH.reindex(columns=columnsList_SLG_LHH).round(3) 
    PT_SLG_RHH = PT_SLG_RHH.reindex(columns=columnsList_SLG_RHH).round(3)
    
    
    PT_OPS_LHH = PT_OPS_LHH.reindex(columns=columnsList_OPS_LHH).round(3)
    PT_OPS_RHH = PT_OPS_RHH.reindex(columns=columnsList_OPS_RHH).round(3)

    

    ##############################################################################
    
    ####################### All dataframes #####################
    
    df_AVG_LHP['Subject'] = 'HitterId'
    df_AVG_RHP['Subject'] = 'HitterId'
    df_OBP_LHP['Subject'] = 'HitterId'
    df_OBP_RHP['Subject'] = 'HitterId'
    df_OPS_LHP['Subject'] = 'HitterId'
    df_OPS_RHP['Subject'] = 'HitterId'
    df_SLG_LHP['Subject'] = 'HitterId'
    df_SLG_RHP['Subject'] = 'HitterId'
    
    HT_AVG_LHP['Subject'] = 'HitterTeamId'
    HT_AVG_RHP['Subject'] = 'HitterTeamId'
    HT_OBP_LHP['Subject'] = 'HitterTeamId'
    HT_OBP_RHP['Subject'] = 'HitterTeamId'
    HT_OPS_LHP['Subject'] = 'HitterTeamId'
    HT_OPS_RHP['Subject'] = 'HitterTeamId'
    HT_SLG_LHP['Subject'] = 'HitterTeamId'
    HT_SLG_RHP['Subject'] = 'HitterTeamId'
    
    PI_AVG_LHH['Subject'] = 'PitcherId'
    PI_AVG_RHH['Subject'] = 'PitcherId'
    PI_OBP_LHH['Subject'] = 'PitcherId'
    PI_OBP_RHH['Subject'] = 'PitcherId'
    PI_OPS_LHH['Subject'] = 'PitcherId'
    PI_OPS_RHH['Subject'] = 'PitcherId'
    PI_SLG_LHH['Subject'] = 'PitcherId'
    PI_SLG_RHH['Subject'] = 'PitcherId'
    
    PT_AVG_LHH['Subject'] = 'PitcherTeamId'
    PT_AVG_RHH['Subject'] = 'PitcherTeamId'
    PT_OBP_LHH['Subject'] = 'PitcherTeamId'
    PT_OBP_RHH['Subject'] = 'PitcherTeamId'
    PT_OPS_LHH['Subject'] = 'PitcherTeamId'
    PT_OPS_RHH['Subject'] = 'PitcherTeamId'
    PT_SLG_LHH['Subject'] = 'PitcherTeamId'
    PT_SLG_RHH['Subject'] = 'PitcherTeamId'

    
    df_AVG_LHP['Stat'] = 'AVG'
    df_AVG_RHP['Stat'] = 'AVG'
    HT_AVG_LHP['Stat'] = 'AVG'
    HT_AVG_RHP['Stat'] = 'AVG'
    PI_AVG_LHH['Stat'] = 'AVG'
    PI_AVG_RHH['Stat'] = 'AVG'
    PT_AVG_LHH['Stat'] = 'AVG'
    PT_AVG_RHH['Stat'] = 'AVG'
    
    df_OBP_LHP['Stat'] = 'OBP'
    df_OBP_RHP['Stat'] = 'OBP'
    HT_OBP_LHP['Stat'] = 'OBP'
    HT_OBP_RHP['Stat'] = 'OBP'
    PI_OBP_LHH['Stat'] = 'OBP'
    PI_OBP_RHH['Stat'] = 'OBP'
    PT_OBP_LHH['Stat'] = 'OBP'
    PT_OBP_RHH['Stat'] = 'OBP'
    
    df_OPS_LHP['Stat'] = 'OPS'
    df_OPS_RHP['Stat'] = 'OPS'
    HT_OPS_LHP['Stat'] = 'OPS'
    HT_OPS_RHP['Stat'] = 'OPS'
    PI_OPS_LHH['Stat'] = 'OPS'
    PI_OPS_RHH['Stat'] = 'OPS'
    PT_OPS_LHH['Stat'] = 'OPS'
    PT_OPS_RHH['Stat'] = 'OPS'

    df_SLG_LHP['Stat'] = 'SLG'
    df_SLG_RHP['Stat'] = 'SLG'
    HT_SLG_LHP['Stat'] = 'SLG'
    HT_SLG_RHP['Stat'] = 'SLG'
    PI_SLG_LHH['Stat'] = 'SLG'
    PI_SLG_RHH['Stat'] = 'SLG'
    PT_SLG_LHH['Stat'] = 'SLG'
    PT_SLG_RHH['Stat'] = 'SLG'

    df_AVG_LHP['PitcherSide'] = 'vs LHP'
    df_OBP_LHP['PitcherSide'] = 'vs LHP'
    df_OPS_LHP['PitcherSide'] = 'vs LHP'
    df_SLG_LHP['PitcherSide'] = 'vs LHP'
    HT_AVG_LHP['PitcherSide'] = 'vs LHP'
    HT_OBP_LHP['PitcherSide'] = 'vs LHP'
    HT_OPS_LHP['PitcherSide'] = 'vs LHP'
    HT_SLG_LHP['PitcherSide'] = 'vs LHP'
    PI_AVG_LHH['PitcherSide'] = 'vs LHH'
    PI_OBP_LHH['PitcherSide'] = 'vs LHH'
    PI_OPS_LHH['PitcherSide'] = 'vs LHH'
    PI_SLG_LHH['PitcherSide'] = 'vs LHH'
    PT_AVG_LHH['PitcherSide'] = 'vs LHH'
    PT_OBP_LHH['PitcherSide'] = 'vs LHH'
    PT_OPS_LHH['PitcherSide'] = 'vs LHH'
    PT_SLG_LHH['PitcherSide'] = 'vs LHH'

    df_AVG_RHP['PitcherSide'] = 'vs RHP'
    df_OBP_RHP['PitcherSide'] = 'vs RHP'
    df_OPS_RHP['PitcherSide'] = 'vs RHP'
    df_SLG_RHP['PitcherSide'] = 'vs RHP'
    HT_AVG_RHP['PitcherSide'] = 'vs RHP'
    HT_OBP_RHP['PitcherSide'] = 'vs RHP'
    HT_OPS_RHP['PitcherSide'] = 'vs RHP'
    HT_SLG_RHP['PitcherSide'] = 'vs RHP'
    PI_AVG_RHH['PitcherSide'] = 'vs RHH'
    PI_OBP_RHH['PitcherSide'] = 'vs RHH'
    PI_OPS_RHH['PitcherSide'] = 'vs RHH'
    PI_SLG_RHH['PitcherSide'] = 'vs RHH'
    PT_AVG_RHH['PitcherSide'] = 'vs RHH'
    PT_OBP_RHH['PitcherSide'] = 'vs RHH'
    PT_OPS_RHH['PitcherSide'] = 'vs RHH'
    PT_SLG_RHH['PitcherSide'] = 'vs RHH'

    
    ##################################################################
    
    cols = ['HitterId', 'Stat', 'PitcherSide', 'Subject','AVG']
    df_AVG_LHP = df_AVG_LHP[cols]
    df_AVG_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    df_AVG_RHP = df_AVG_RHP[cols]
    df_AVG_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    
    cols = ['HitterId', 'Stat', 'PitcherSide', 'Subject','OBP']
    df_OBP_LHP = df_OBP_LHP[cols]
    df_OBP_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    df_OBP_RHP = df_OBP_RHP[cols]
    df_OBP_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    
    cols = ['HitterId', 'Stat', 'PitcherSide', 'Subject','OPS']
    df_OPS_LHP = df_OPS_LHP[cols]
    df_OPS_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    df_OPS_RHP = df_OPS_RHP[cols]
    df_OPS_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['HitterId', 'Stat', 'PitcherSide', 'Subject','SLG']
    df_SLG_LHP = df_SLG_LHP[cols]
    df_SLG_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    df_SLG_RHP = df_SLG_RHP[cols]
    df_SLG_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    cols = ['HitterTeamId', 'Stat', 'PitcherSide', 'Subject','AVG']
    HT_AVG_LHP = HT_AVG_LHP[cols]
    HT_AVG_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    HT_AVG_RHP = HT_AVG_RHP[cols]
    HT_AVG_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['HitterTeamId', 'Stat', 'PitcherSide', 'Subject','OBP']
    HT_OBP_LHP = HT_OBP_LHP[cols]
    HT_OBP_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    HT_OBP_RHP = HT_OBP_RHP[cols]
    HT_OBP_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['HitterTeamId', 'Stat', 'PitcherSide', 'Subject','OPS']
    HT_OPS_LHP = HT_OPS_LHP[cols]
    HT_OPS_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    HT_OPS_RHP = HT_OPS_RHP[cols]
    HT_OPS_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['HitterTeamId', 'Stat', 'PitcherSide', 'Subject','SLG']
    HT_SLG_LHP = HT_SLG_LHP[cols]
    HT_SLG_LHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    HT_SLG_RHP = HT_SLG_RHP[cols]
    HT_SLG_RHP.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherId', 'Stat', 'PitcherSide', 'Subject','AVG']
    PI_AVG_LHH = PI_AVG_LHH[cols]
    PI_AVG_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PI_AVG_RHH = PI_AVG_RHH[cols]
    PI_AVG_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherId', 'Stat', 'PitcherSide', 'Subject','OBP']
    PI_OBP_LHH = PI_OBP_LHH[cols]
    PI_OBP_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PI_OBP_RHH = PI_OBP_RHH[cols]
    PI_OBP_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherId', 'Stat', 'PitcherSide', 'Subject','OPS']
    PI_OPS_LHH = PI_OPS_LHH[cols]
    PI_OPS_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PI_OPS_RHH = PI_OPS_RHH[cols]
    PI_OPS_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherId', 'Stat', 'PitcherSide', 'Subject','SLG']
    PI_SLG_LHH = PI_SLG_LHH[cols]
    PI_SLG_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PI_SLG_RHH = PI_SLG_RHH[cols]
    PI_SLG_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']


    cols = ['PitcherTeamId', 'Stat', 'PitcherSide', 'Subject','AVG']
    PT_AVG_LHH = PT_AVG_LHH[cols]
    PT_AVG_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PT_AVG_RHH = PT_AVG_RHH[cols]
    PT_AVG_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherTeamId', 'Stat', 'PitcherSide', 'Subject','OBP']
    PT_OBP_LHH = PT_OBP_LHH[cols]
    PT_OBP_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PT_OBP_RHH = PT_OBP_RHH[cols]
    PT_OBP_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherTeamId', 'Stat', 'PitcherSide', 'Subject','OPS']
    PT_OPS_LHH = PT_OPS_LHH[cols]
    PT_OPS_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PT_OPS_RHH = PT_OPS_RHH[cols]
    PT_OPS_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    
    cols = ['PitcherTeamId', 'Stat', 'PitcherSide', 'Subject','SLG']
    PT_SLG_LHH = PT_SLG_LHH[cols]
    PT_SLG_LHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']
    PT_SLG_RHH = PT_SLG_RHH[cols]
    PT_SLG_RHH.columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    

    dataframes = [df_AVG_LHP,
                  df_AVG_RHP,
                  df_OBP_LHP,
                  df_OBP_RHP,
                  df_OPS_LHP,
                  df_OPS_RHP,
                  df_SLG_LHP,
                  df_SLG_RHP,
                  HT_AVG_LHP,
                  HT_AVG_RHP,
                  HT_OBP_LHP,
                  HT_OBP_RHP,
                  HT_OPS_LHP,
                  HT_OPS_RHP,
                  HT_SLG_LHP,
                  HT_SLG_RHP,
                  PI_AVG_LHH,
                  PI_AVG_RHH,
                  PI_OBP_LHH,
                  PI_OBP_RHH,
                  PI_OPS_LHH,
                  PI_OPS_RHH,
                  PI_SLG_LHH,
                  PI_SLG_RHH,
                  PT_AVG_LHH,
                  PT_AVG_RHH,
                  PT_OBP_LHH,
                  PT_OBP_RHH,
                  PT_OPS_LHH,
                  PT_OPS_RHH,
                  PT_SLG_LHH,
                  PT_SLG_RHH]

    
    dataframe_combined = pd.concat(dataframes)
       
    output = dataframe_combined.sort_values(['SubjectId', 'Stat', 'Split', 'Subject'], ascending = True)       
      
    output.to_csv('./data/processed/output.csv', index=False) 
    
    

if __name__ == '__main__':
    main()
